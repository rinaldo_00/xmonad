# Dotfiles for WM

This repo is my personal dotfiles of window managers Xmonad, Bspwm, Qtile and its dependencies.

## Installation of this dotfiles

Just copy paste the dotfiles to your home directory.

Go to your home directory and execute the following command in the shell

```
git clone https://gitlab.com/nikilson/dotfiles.git
```
```
cp -R dotfiles/* ~/
```

# Fonts 

Various fonts are used here after importing this dotfiles make sure you installed all the required fonts for displaying the icons and emojis in the polybar and xombarrc

## Fonts used for Polybar

- Jet Brains Mono Medium
- Hack Nerd Font
- Noto Color Emoji
- IPAGothic

## Fonts used for Xmobar

- Ubuntu Fonts
- Mononoki
- Font Awesome

# Installation of fonts

## For Debian based distros

- Some of the fonts are available in repository. You can simply install them with apt package manager
- If the font is not available download the manually in web and copy and paste the manually in a following directory

```
/usr/share/fonts/
```

## For Arch based distros

- Simply search the fonts in AUR (Arch User Repository) and install with yay package manager.
- You can install them manually similarly like debian based distros. Copy and paste in the following directory.

```
/usr/share/fonts/
```

# Thank you 

These config files are based the following peoples dotfiles.

- Distrotube
- Brodie Robertson
- Chris Titus Tech

I really thank them for their source

I thank you for selecting my dotfiles

## Support

Your contribution to this project is fixing bugs and reporting issues.
